import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Scanner;

public class StatisticsCustomHash {
	
	static CustomHash hashmap = new CustomHash();
	static Scanner Input = new Scanner(System.in);
	static String personName, game;
	private static BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));

	public static void inputPersonAndHisGameOfInterest() throws IOException {
		String Line = bufferReader.readLine();
		personName = Line.split(" ")[0];
		game = Line.split(" ")[1];
        
		if (hashmap.ContainsKey(game) == true) {
			int value = (int) hashmap.get(game);
			hashmap.put(game, ++value);
		} else {
			hashmap.put(game, 1);
		}
	}
	
	private static void footBallAsFavSport() {
		String Key = "football";
		if (hashmap.ContainsKey(Key) == true) {
			int value = (int) hashmap.get(Key);
			System.out.println(value);
		} else {
			System.out.println("0");
		}

	}
	
    
	public static int display()
	 {
		int max = 0;
	 for(int i=0 ; i < hashmap.ARR_SIZE ; i++)
	 {
	  if(hashmap.arr[i] == null)
	  {
	   continue;
	  }
	  else{
		  
		  if(max < (int)hashmap.arr[i].element().value)
          {
              max = (int)hashmap.arr[i].element().value;
          }
	   System.out.println(hashmap.arr[i].element().key);
	   System.out.println(hashmap.arr[i].element().value);
	  }
	  
	 }
	 return max;
	 }
	
	public static void main(String args[]) throws NumberFormatException, IOException {
		int iteration = Integer.parseInt(bufferReader.readLine());

		while (iteration-- > 0) {
			inputPersonAndHisGameOfInterest();

		}
		//System.out.println(findMostlikedGame());
		footBallAsFavSport();
		int maximum=display();
		System.out.println(hashmap.get(maximum));
		

	}
}
