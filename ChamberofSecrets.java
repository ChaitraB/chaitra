import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

class spiders 
{
	private int spiderIndex;
	private int spiderPower;

	spiders(int spiderIndex, int spiderPower) {
		this.spiderIndex = spiderIndex;
		this.spiderPower = spiderPower;
	}

	public int getSpiderIndex() {
		return spiderIndex;
	}

	public void setSpiderIndex(int spiderIndex) {
		this.spiderIndex = spiderIndex;
	}

	public int getSpiderPower() {
		return spiderPower;
	}

	public void setSpiderPower(int spiderPower) {
		this.spiderPower = spiderPower;
	}

	@Override
	public String toString() {
		return "spiders [spiderIndex=" + spiderIndex + ", spiderPower=" + spiderPower + "]";
	}
	
}

class arogan {

	static int GetMaxPower(List<spiders> temp, int selectSpider)
	{
		
		int length = temp.size();
		if (length < selectSpider) {
			selectSpider = length;
		}
		int[] dequedSpider = new int[selectSpider];
		for (int i = 0; i < selectSpider; i++) {
			dequedSpider[i] = temp.get(i).getSpiderPower();
		}
		int maxPower = findLargestPowerSpider(dequedSpider);

		return maxPower;
	}

	private static int findLargestPowerSpider(int[] dequedSpider) {
		int max = dequedSpider[0];
		for (int i = 1; i < dequedSpider.length; i++) {
			if (dequedSpider[i] > max) {
				max = dequedSpider[i];
			}
		}
		return max;
	}

	static boolean NumberOfSpidersInRange(int noOfSpiders, int selectSpider) {
		int maxNoOfSpiders;
		maxNoOfSpiders = selectSpider * selectSpider;
		if (noOfSpiders >= selectSpider || noOfSpiders <= maxNoOfSpiders) {
			return true;
		} else {
			return false;
		}
	}

	static boolean SelectedSpidersInRange(int selectSpider) {
		int MinSelectedSpiders = 1;
		int MaxSelectedSpiders = 316;
		if (selectSpider >= MinSelectedSpiders && MaxSelectedSpiders >= selectSpider) {
			return true;
		} else
			return false;
	}

	

	public static List<spiders> RequeSpider(int maxPower, List<spiders> totalSpiders, List<spiders> temp)
	{
		for (Iterator iterator = temp.iterator(); iterator.hasNext();)
		{
			spiders spiders = (spiders) iterator.next();
			if (spiders.getSpiderPower() == maxPower) 
			{
				System.out.println(spiders.getSpiderIndex());
				iterator.remove();
			}
			
	     }
		for (int i = 0; i < temp.size(); i++) 
		{
			int index = temp.get(i).getSpiderIndex();
			int power = temp.get(i).getSpiderPower();
			if (power > 0) 
			{
				totalSpiders.add(new spiders(index, --power));
			} 
			else 
			{
				totalSpiders.add(new spiders(index, power));
			}
		}
		return totalSpiders;
	}
	
	public static List<spiders> getSublist(List<spiders> originalList, int fromIndex, int toIndex){
		List<spiders> subList = new ArrayList<>();
		for (int i = 0; i < originalList.size(); i++)
		{
			if(i >= fromIndex && i < toIndex)
			{
				subList.add(originalList.get(i));
			}
		}
		
		return subList;
	}
}

public class ChamberofSecrets 
{

	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		List<spiders> totalSpiders = new ArrayList<spiders>();
		int noOfSpiders, selectSpider;
		noOfSpiders = input.nextInt();
		selectSpider = input.nextInt();
		if (arogan.NumberOfSpidersInRange(noOfSpiders, selectSpider) && arogan.SelectedSpidersInRange(selectSpider))
		{

			for (int i = 1; i <= noOfSpiders; i++) 
			{
				int spiderPower = input.nextInt();
				totalSpiders.add(new spiders(i, spiderPower));
			}
		}
		for (int i = 0; i < selectSpider; i++)
		{
			if (totalSpiders.size() < selectSpider) 
			{
				selectSpider = totalSpiders.size();
			}			
			List<spiders> temp = arogan.getSublist(totalSpiders, 0,selectSpider);
			totalSpiders.subList(0, selectSpider).clear();
			int maxPower = arogan.GetMaxPower(temp, selectSpider);
			totalSpiders = arogan.RequeSpider(maxPower,totalSpiders,temp);
			
		}

	}
	
	
}