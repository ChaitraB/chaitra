import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class RemoveFriends {
	private static LinkedList<Integer> friendsList = new LinkedList<Integer>();
	private static BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
	private static int totalFriends, noOfFrndsToRemove;

	private static void removeFriends() {

		for (int frnds = 1; frnds <= noOfFrndsToRemove; frnds++) {
			boolean DeleteFriend = false;
			for (int i = 0; i < friendsList.size() - 1; i++) {
				if (friendsList.get(i) < friendsList.get(i + 1)) {
					friendsList.remove(i);
					DeleteFriend = true;
					break;
				}
			}
			if (DeleteFriend == false) {
				friendsList.remove(friendsList.size() - 1);
			}
		}
	}

	private static void displayFriends() {
		for (int i = 0; i < friendsList.size(); i++) {
			System.out.print(friendsList.get(i) + " ");
		}
		System.out.println();
	}

	private static boolean iterationInRange(int iteration) {
		if (iteration >= 0 && iteration <= 1000)
			return true;
		else

			return false;
	}

	private static boolean totalFriendsInRange(int totalFriends) {
		if (totalFriends >= 1 && totalFriends <= 100000)
			return true;
		else
			return false;
	}

	private static boolean noOfFrndToRemoveInRnage(int noOfFrndsToRemove, int totalFriends) {
		if (noOfFrndsToRemove >= 0 && noOfFrndsToRemove < totalFriends)
			return true;
		else
			return false;
	}

	private static String[] getFriendsPopularity() throws IOException {
		return bufferReader.readLine().split(" ");
	}

	private static void loadAndRremoveFriends(String[] friendsPopularityInput) {
		for (int i = 0; i < totalFriends; i++) {
			int currentFriendPopularity = Integer.parseInt(friendsPopularityInput[i]);
			while (!friendsList.isEmpty() && noOfFrndsToRemove != 0
					&& friendsList.getLast() < currentFriendPopularity) {

				{
					friendsList.removeLast();
					noOfFrndsToRemove--;
				}
			}
			friendsList.addLast(currentFriendPopularity);
		}

		if (noOfFrndsToRemove > 0)
		{
			removeFriends();
		}
	}

	public static void main(String args[]) throws IOException {

		int iteration = Integer.parseInt(bufferReader.readLine());
		if (iterationInRange(iteration)) {
			while(iteration-->0)
			{
				String input = bufferReader.readLine();

				totalFriends = Integer.parseInt(input.split(" ")[0]);
				noOfFrndsToRemove = Integer.parseInt(input.split(" ")[1]);

				if (totalFriendsInRange(totalFriends) && noOfFrndToRemoveInRnage(noOfFrndsToRemove, totalFriends)) {
					friendsList.removeAll(friendsList);
					loadAndRremoveFriends(getFriendsPopularity());
					displayFriends();

				}
			}
		}
	}

}
