import static org.junit.Assert.*;

import org.junit.Test;

public class MatrixAdditionTest {

	MatrixAddition calculate = new MatrixAddition();
	@Test
	public void isValidTest()
	{    
        boolean expected = true;

        //Act
        boolean isValid = calculate.isValid(6);

        //Assert
		assertEquals(expected,isValid);
	}
	
	@Test
	public void oddIndexSumTest()
	{
        int expected = 25;

        //Act
       int oddSum = calculate.oddAlternateSum();

        //Assert
		assertEquals(expected,oddSum);
	}
	
	@Test
	public void evenIndexSumTest()
	{
		
        int expected = 15;

        //Act
       int evenSum = calculate.evenAlternateSum();

        //Assert
    		assertEquals(expected,evenSum);
		
	}

}

