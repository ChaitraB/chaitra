import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class Statistics {
	static HashMap<String, Integer> hashmap = new HashMap<String, Integer>();
	static Scanner Input = new Scanner(System.in);
	static String personName, game;
	private static BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));

	public static void inputPersonAndHisGameOfInterest() throws IOException {
		String Line = bufferReader.readLine();
		personName = Line.split(" ")[0];
		game = Line.split(" ")[1];

		if (hashmap.containsKey(game) == true) {
			int value = (int) hashmap.get(game);
			hashmap.replace(game, ++value);
		} else {
			hashmap.put(game, 1);
		}
	}

	private static String findMostlikedGame() {

		Entry<String, Integer> maxEntry = null;

		for (Entry<String, Integer> entry : hashmap.entrySet()) {

			if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
				maxEntry = entry;
			}
		}
		return maxEntry.getKey();

	}

	private static void footBallAsFavSport() {
		String Key = "football";
		if (hashmap.containsKey(Key) == true) {
			int value = hashmap.get(Key);
			System.out.println(value);
		} else {
			System.out.println("0");
		}

	}

	public static void main(String args[]) throws NumberFormatException, IOException {
		int iteration = Integer.parseInt(bufferReader.readLine());

		while (iteration-- > 0) {
			inputPersonAndHisGameOfInterest();

		}
		System.out.println(findMostlikedGame());
		footBallAsFavSport();

	}

}
