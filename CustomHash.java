import java.util.LinkedList;
import java.lang.Math;

public class CustomHash {

	public static final int ARR_SIZE = 128;
	LinkedList<Bucket>[] arr = new LinkedList[ARR_SIZE];

	public static class Bucket {
		public Object key;
		public Object value;

		public Object getKey() {
			return key;
		}

		public void setKey(Object key) {
			this.key = key;
		}

		public Object getValue() {
			return value;
		}

		public void setValue(Object value) {
			this.value = value;
		}

	}

	public CustomHash() {
		for (int i = 0; i < ARR_SIZE; i++) {
			arr[i] = null;
		}
	}

	public Object get(Object key) {
		Bucket item = getObj(key);

		if (item == null)
			return null;
		else
			/*
			System.out.println(item.getKey());
		    System.out.println(item.getValue());*/
			return item.value;
		
	}

	private Bucket getObj(Object key) {
		if (key == null)
			return null;

		int index = key.hashCode() % ARR_SIZE;
		LinkedList<Bucket> items = arr[Math.abs(index)];

		if (items == null)
			return null;

		for (Bucket item : items) {
			if (item.key.equals(key))
				return item;
		}

		return null;
	}

	public void put(Object key, Object value) {
		int index = key.hashCode() % ARR_SIZE;
		LinkedList<Bucket> items = arr[Math.abs(index)];

		if (items == null) {
			items = new LinkedList<Bucket>();

			Bucket item = new Bucket();
			item.key = key;
			item.value = value;

			items.add(item);

			arr[Math.abs(index)] = items;
		} else {
			for (Bucket item : items) {
				if (item.key.equals(key)) {
					item.value = value;
					return;
				}
			}

			Bucket item = new Bucket();
			item.key = key;
			item.value = value;

			items.add(item);
		}
	}

	public void display() {
		for (int i = 0; i < arr.length; i++) {
			
			if(arr[i]== null)
			{
				continue;
			}
			else
			{
				LinkedList<Bucket> item = arr[i];
				System.out.println(item.get(i));
			}
			
		}
		
	}

	public boolean ContainsKey(Object key) {
		int index1 = key.hashCode() % ARR_SIZE;
		if (arr[Math.abs(index1)] == null) {
			return false;
		}
		return true;
	}

	public static void main(String args[]) {
		CustomHash hm = new CustomHash();
		/*hm.put("cricket", 1);
		hm.put("cricket", 4);
		System.out.println(hm.get("cricket"));
		hm.put("football", "1");
		System.out.println(hm.get("football"));
		hm.display();*/
	}

}