import static org.junit.Assert.*;
import org.junit.Test;

public class BstTest {
	BSTHeight bst = new BSTHeight();
	BSTHeight.TreeNode node;
	@Test
	public void insert() {
		bst.insertElementIntoTree(5);
		bst.insertElementIntoTree(3);
		bst.insertElementIntoTree(4);
		bst.insertElementIntoTree(2);
		bst.insertElementIntoTree(8);
		bst.insertElementIntoTree(6);
	}
	
	@Test
	public void test_height() {
		System.out.println("In is Height");
		assertEquals(bst.heightOfBinaryTree(BSTHeight.rootNodeOfTree), 3);
		assertEquals(bst.heightOfBinaryTree(null), 0);
		assertNotEquals(bst.heightOfBinaryTree(BSTHeight.rootNodeOfTree), 1);
	}

	@Test
	public void test_nodeValues()
	{
		System.out.println("Root node");
		node=bst.insertAccordingToNodeValue(null,  10);
		assertEquals(node.nodeValue,10);
		node=bst.insertAccordingToNodeValue(node,  15);
		assertEquals(node.rightChild.nodeValue,15);
		node=bst.insertAccordingToNodeValue(node,  8);
		assertEquals(node.leftChild.nodeValue,8);
	}
	
	@Test
	public void test_isValid_Range() {
		System.out.println("Is Valid N value");
		assertEquals(bst.isValidRange(10), true);
		assertEquals(bst.isValidRange(10001), false);
	}

	@Test
	public void test_isArrayValueInRange() {
		System.out.println("In Is array value in Range");
		assertEquals(bst.isValidArrayValue(10), true);
		assertEquals(bst.isValidArrayValue(1000001), false);
	}

}
