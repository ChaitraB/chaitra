import java.util.*;
 
class BSTHeight{
 public static TreeNode rootNodeOfTree;
 public static Scanner input = new Scanner(System.in);
 
 public class TreeNode {
  int nodeValue;
  TreeNode leftChild, rightChild;
 
  public TreeNode(int item) {
   nodeValue = item;
   leftChild = rightChild = null;
  }
 }
 
 BSTHeight() {
  rootNodeOfTree = null;
 }
 
 public void insertElementIntoTree(int nodeValue) {
  rootNodeOfTree = insertAccordingToNodeValue(rootNodeOfTree, nodeValue);
 }
 
 public TreeNode insertAccordingToNodeValue(TreeNode rootNode, int nodeValue) {
 
  if (rootNode == null) {
   rootNode = new TreeNode(nodeValue);
   return rootNode;
  }
 
  if (nodeValue <= rootNode.nodeValue)
   rootNode.leftChild = insertAccordingToNodeValue(rootNode.leftChild, nodeValue);
  else if (nodeValue > rootNode.nodeValue)
   rootNode.rightChild = insertAccordingToNodeValue(rootNode.rightChild, nodeValue);
 
  return rootNode;
 }
 
 public int heightOfBinaryTree(TreeNode node) {
  if (node == null) {
   return 0;
  } else {
   return 1 + Math.max(heightOfBinaryTree(node.leftChild), heightOfBinaryTree(node.rightChild));
  }
 }
 
 public static void main(String[] args) {
	 BSTHeight bst = new BSTHeight();
 
  int numberOfElements = input.nextInt();
  if(bst.isValidRange(numberOfElements))
  {
  for (int nodeNumber = 0; nodeNumber < numberOfElements; nodeNumber++) {
     int arrayValue=input.nextInt();
       if(bst.isValidArrayValue(arrayValue))
       {
   bst.insertElementIntoTree(arrayValue);
       }
  }
  System.out.println(bst.heightOfBinaryTree(rootNodeOfTree));
   }
 }

public boolean isValidArrayValue(int arrayValue) {
	if(arrayValue<=100000)
		return true;
	else
		return false;
}

public boolean isValidRange(int numberOfElements) {
	if(numberOfElements <= 1000)
		return true;
	else
		return false;
}

}