import java.util.Scanner;

public class SpiderQuestion {
	
	public static int NumberOfSpiders , SelectedSpiders , SpiderPower , LargestPower , LargestPowerPosition ;
	public static int[] SelectedSpiderPower = new int[316] , SelectedSpiderPosition= new int[316] ;
	public Queue PowerOfSpider =new Queue();
	public Queue InitialPositionOfSpider=new Queue();
	public boolean NumberOfSpidersInRange()
	{
	  int MinNumberOfSpiders = SelectedSpiders;
	  int MaxNumberOfSpiders = (SelectedSpiders*SelectedSpiders);
	 if(NumberOfSpiders>=MinNumberOfSpiders && MaxNumberOfSpiders>=NumberOfSpiders)
	 {
		 return true;
	 }else
		 return false;
	}
	boolean SelectedSpidersInRange()
	{
	  int MinSelectedSpiders = 1;
	 int MaxSelectedSpiders = 316;
	 if(SelectedSpiders>=MinSelectedSpiders && MaxSelectedSpiders>=SelectedSpiders)
	 {
		 return true;
	 }else
		 return false;
	}
	boolean SpiderPowerInRange()
	{
	 int MinSpiderPower = 1;
	  int MaxSpiderPower = 316;
	 if(SpiderPower>=MinSpiderPower && MaxSpiderPower>=SpiderPower)
	 {
		 return true;
	 }else
		 return false;
	}
	public void EnQueueSpiders()
	{
	 for(int i=0;i<NumberOfSpiders;i++)
	    {
		 Scanner input = new Scanner(System.in);
		
	     SpiderPower = input.nextInt();
	     //System.out.println(SpiderPower);
	     if(SpiderPowerInRange())
	     {
	      PowerOfSpider.EnQueue(SpiderPower);
	      InitialPositionOfSpider.EnQueue(i+1); 
	     }
	    }
	}
	public void DeQueueSpiders()
	{
	 for(int j=0;j<SelectedSpiders;j++)
	 {
	  SelectedSpiderPower[j] = PowerOfSpider.DeQueue();
	  SelectedSpiderPosition[j]=InitialPositionOfSpider.DeQueue();
	 }
	}
	void FindLargestPowerSpider()
	{
	 LargestPower = SelectedSpiderPower[0];
	 LargestPowerPosition = 0;
	 for(int j=1;j<SelectedSpiders;j++)
	 {
	  if(LargestPower==SelectedSpiderPower[j])
	   continue;
	  if(LargestPower<SelectedSpiderPower[j])
	  {
	   LargestPower=SelectedSpiderPower[j];
	   LargestPowerPosition=j;
	  }
	 }
	}
	void DecrementPower()
	{
	 for(int j=0;j<SelectedSpiders;j++)
	 {
	  if(SelectedSpiderPower[j]>0)
	   SelectedSpiderPower[j]--;
	 }
	}
	void ReQueueSelectedSpiders()
	{
	 for(int j=0;j<SelectedSpiders;j++)
	 {
	  if(j==LargestPowerPosition)
	   continue;
	  else
	  {
	   PowerOfSpider.EnQueue(SelectedSpiderPower[j]);
	   InitialPositionOfSpider.EnQueue(SelectedSpiderPosition[j]);
	  }
	 }
	}
	public static void main(String args[])
	{
		SpiderQuestion obj= new SpiderQuestion();
		Scanner input = new Scanner(System.in);
		 NumberOfSpiders=input.nextInt();
		 SelectedSpiders=input.nextInt();
	    if(obj.NumberOfSpidersInRange() && obj.SelectedSpidersInRange())
	    {
	     obj.EnQueueSpiders();
	     for(int i=0;i<SelectedSpiders;i++)
	     {
	      obj.DeQueueSpiders();
	      obj.FindLargestPowerSpider();
	      obj.DecrementPower();
	      obj.ReQueueSelectedSpiders();
	      System.out.println(SelectedSpiderPosition[LargestPowerPosition]);
	     }

	}
}
}